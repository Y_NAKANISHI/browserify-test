import 'jquery';
import moduleSub from "./moduleSub";


export default class Module1{
    constructor() {
        //自身のメソッドを実行
        this.callStart();
        new moduleSub();
    }

    //メソッド
    callStart() {
        alert("池袋");
        $(".target").html("変更しました");
    }
}

