/**
 * Created by neralty on 2017/03/28.
 */
import $ from "jquery";
import "velocity-animate"
export default class moduleSub {
	constructor() {
		this.addScrollEvent();
	}

	addScrollEvent() {
		//ScrollMagic のコントローラーを作成する。
		//{addIndicators: true}でデバグ用のインディケーターを表示する
		//↓をインストールする必要あり
		//<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.js"></script>
		const controller = new ScrollMagic.Controller({addIndicators: true});

		// シーンインスタンスを作成して、addTo(コントローラー名);で追加していく
		//
		new ScrollMagic.Scene({
			//トリガー位置の対象となるエレメント DOMでもjQuery的なid,class指定もOK
			triggerElement: "#trigger1",
			//トリガーするウィンドウの位置。センターならウィンドウの真ん中がトリガーに
			triggerHook: "onCenter",
			//アニメーションの逆再生を許可。falseにすれば、一旦アニメーションしたものは、ブラウザを上げていっても逆再生しない
			reverse: true,
			//
		})
		//ベロシティアニメーションをトリガー
		//<script src="./js/animation.velocity.min.js"></script>を追加していること
			.setVelocity("#trigger1", {opacity: 0}, {duration: 1000})
			//コントローラーに作ったシーンを追加
			.addTo(controller);

		new ScrollMagic.Scene({
			triggerElement: "#trigger2",
			//ブラウザの上端でトリガー。つまり画面からきえそうになると発動
			triggerHook: "onCenter",
			//リバースしない
			reverse: false,
			//200px遅らせる
			offset: 200
		})
			.setVelocity("#trigger2", {opacity: 0}, {duration: 1000})
			.addTo(controller);

		//.manyAnimationTargetをつけたDOMを複数に一気にアニメーションを指定する
		$(".manyAnimationTarget").each((index, element) => { //eachメソッドの返り値？としてえられるelementは生のDOM
			//なのでjQueryオブジェクトにするためには以下の処理を
			const $element = $(element);
			const offset = $element.height() / 2;

			//import "velocity-animate"で入れた場合Velocityという関数が使える。
			//$element.velocityという書き方はバグる
			Velocity(element, { opacity: 0.5 }, 1000); // Velocity

			// $element.css('opacity', '0');

			new ScrollMagic.Scene({
				//生のDOMをそのまま対象に
				triggerElement: element,
				triggerHook: "onCenter",
				offset: offset,
				reverse: true
			})
				.setVelocity(element, {
					opacity: 1,
				}, {
					duration: 400,
					easing: "easeInOutCubic"
				})
				.addTo(controller);
		});
	}
}
