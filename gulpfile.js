//まずはnpmをインポートする
const gulp = require('gulp');
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");

//Browserify + babelify
gulp.task('build:scripts', function() {
    return browserify('./main.js', { debug: true })
        .transform("babelify", {presets: ["es2015"]})
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./'));
});

//デフォルトで実行されるタスクに'build:scripts'タスクを登録
//gulp で実行すると使える
gulp.task('default', ['build:scripts']);



// //以下コピペ
// var gulp = require('gulp');
// var sourcemaps = require('gulp-sourcemaps');
// var source = require('vinyl-source-stream');
// var buffer = require('vinyl-buffer');
// var browserify = require('browserify');
// var watchify = require('watchify');
// var babelify = require('babelify');
//
// function compile(watch) {
//     var bundler = watchify(browserify('./main.js', { debug: true }).transform("babelify", {presets: ["es2015"]}));
//
//     function rebundle() {
//         bundler.bundle()
//             .on('error', function(err) { console.error(err); this.emit('end'); })
//             .pipe(source('app.js'))
//             .pipe(buffer())
//             .pipe(sourcemaps.init({ loadMaps: true }))
//             .pipe(sourcemaps.write('./'))
//             .pipe(gulp.dest('./'));
//     }
//
//     if (watch) {
//         bundler.on('update', function() {
//             console.log('-> bundling...');
//             rebundle();
//         });
//     }
//
//     rebundle();
// }
//
// function watch() {
//     return compile(true);
// }
//
// gulp.task('build', function() { return compile(); });
// gulp.task('watch', function() { return watch(); });
//
// gulp.task('default', ['watch']);
// //コピペ